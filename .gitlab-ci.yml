stages:
- containers
- build
- tests
- release
- publish

include:
  - project: 'freedesktop/ci-templates'
    ref: e9a461877f8c7f9fed9fff5491067ec3c3472559
    file:
      - '/templates/arch.yml'

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  BASE_TAG: "2023-10-11.1"
  ALPINE_VERSION: "3.18"  # FIXME: remove any mention of edge (for riscv) when 3.18 gets released
  UROOT_CONTAINER_SUFFIX: "u-root"

# Run pipelines on valve-infra CI gateways, not to waste fd.o machine time
default:
  tags:
    - CI-gateway
    - cpu:arch:x86_64

# Only run jobs post-merge or in merge requests
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_COMMIT_TAG

.linux-build-container:
  variables:
    FDO_DISTRIBUTION_TAG: '2023-11-23-linux-build'


# STAGE: containers


uroot:
  timeout: 2h
  parallel:
    matrix:
      - GOARCH: amd64
      - GOARCH: arm64
     #- GOARCH: arm
      - GOARCH: riscv64
  image: registry.freedesktop.org/freedesktop/ci-templates/x86_64/container-build-base:2023-11-24.1
  stage: containers
  variables:
    IMAGE_NAME: $CI_REGISTRY_IMAGE/$UROOT_CONTAINER_SUFFIX:$GOARCH-$BASE_TAG
  script: .gitlab-ci/u-root-container-build.sh

linux_build_container:
  extends:
    - .fdo.container-build@arch
    - .linux-build-container
  stage: containers
  variables:
     FDO_DISTRIBUTION_PACKAGES: 'base-devel bc cpio curl git wget tar aarch64-linux-gnu-gcc arm-none-eabi-gcc riscv64-linux-gnu-gcc yq rdfind'

uroot combine:
  image: registry.freedesktop.org/freedesktop/ci-templates/x86_64/container-build-base:2023-11-24.1
  stage: containers
  needs:
    - job: uroot
      artifacts: false
  variables:
    FDO_DISTRIBUTION_TAG: "$BASE_TAG"
    FDO_REPO_SUFFIX: "$UROOT_CONTAINER_SUFFIX"
    FDO_GOARCHES: amd64 arm64 riscv64
  script:
    # log in to the registry
    - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

    - for GOARCH in $FDO_GOARCHES ;
      do
        IMAGES="$CI_REGISTRY_IMAGE/${FDO_REPO_SUFFIX}:${GOARCH}-${FDO_DISTRIBUTION_TAG} ${IMAGES}" ;
      done

    # create the multi-arch manifest
    - buildah manifest create ${FDO_REPO_SUFFIX}:${FDO_DISTRIBUTION_TAG} ${IMAGES}

    # check if we already have this manifest in the registry
    - buildah manifest inspect ${FDO_REPO_SUFFIX}:${FDO_DISTRIBUTION_TAG} > new_manifest.json
    - buildah manifest inspect docker://${CI_REGISTRY_IMAGE}/${FDO_REPO_SUFFIX}:${FDO_DISTRIBUTION_TAG} > current_manifest.json || true

    - diff -u current_manifest.json new_manifest.json || touch .need_push

    # and push it
    - |
      if [[ -e .need_push ]]
      then
        rm .need_push
        buildah manifest push --format v2s2 --all \
              ${FDO_REPO_SUFFIX}:${FDO_DISTRIBUTION_TAG} \
              docker://${CI_REGISTRY_IMAGE}/${FDO_REPO_SUFFIX}:${FDO_DISTRIBUTION_TAG}
      fi


# STAGE: build


.run_in_container:
  image: !reference [uroot, variables, IMAGE_NAME]
  artifacts:
    paths:
      - releases/

build amd64:
  stage: build
  extends:
    - .run_in_container
  variables:
    GOARCH: amd64
    GIT_DEPTH: 0
    GIT_STRATEGY: clone  # depth 0 is broken with fetch, see https://gitlab.com/gitlab-org/gitlab/-/issues/292470
  needs:
    - job: uroot
      artifacts: false
      parallel:
        matrix:
          - GOARCH: amd64
  script:
    - |
      if $(git rev-parse --is-shallow-repository)
      then
        echo >&2 "The repository's full git history needs to be present in order for \`git describe\` to be reliable."
        exit 1
      fi

    - B2C_VERSION="$(git describe --dirty --always --tags)" sh ./container/entrypoint.sh

    - xz --threads=0 --check=crc32 -9 --lzma2=dict=1MiB --stdout /tmp/initramfs.linux_${GOARCH}.cpio | dd conv=sync bs=512 of=/tmp/initramfs.linux_${GOARCH}.cpio.xz
    - mkdir -p releases/$CI_COMMIT_BRANCH/; cp /tmp/*.cpio.xz releases/$CI_COMMIT_BRANCH/

build arm64:
  extends:
    - build amd64
  variables:
    GOARCH: arm64
  tags:
    - aarch64
  needs:
    - job: uroot
      artifacts: false
      parallel:
        matrix:
          - GOARCH: arm64

.build arm:
  extends:
    - build arm64
  variables:
    GOARCH: arm
    GOARM: 6
  needs:
    - job: uroot
      artifacts: false
      parallel:
        matrix:
          - GOARCH: arm

build riscv64:
  extends:
    - build amd64
  variables:
    GOARCH: riscv64
  needs:
    - job: uroot
      artifacts: false
      parallel:
        matrix:
          - GOARCH: riscv64

linux:
  parallel:
    matrix:
      - GOARCH: amd64
        LINUX_ARCH: x86_64
      - GOARCH: arm64
        LINUX_ARCH: arm64
     #- GOARCH: arm
     #  LINUX_ARCH: arm
      - GOARCH: riscv64
        LINUX_ARCH: riscv64
  stage: build
  extends:
    - .fdo.distribution-image@arch
    - .linux-build-container
  needs:
    - job: linux_build_container
      artifacts: false
  rules:
    - if: $CI_COMMIT_TAG
    - changes:
      - config/linux/*
  variables:
    LINUX_REPO: https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git
    LINUX_BRANCH: linux-6.6.y
    BASE_FEATURES: common,qemu,b2c_logo
    FULL_FEATURES: $BASE_FEATURES,netfilter,network,sensors,serial_adapters,kvm,gpu,wifi
  artifacts:
    paths:
      - releases/
  before_script: |
    env
    git clone --depth 1 -b $LINUX_BRANCH $LINUX_REPO deps/linux
    git config --global user.name "Friendly CI"
    git config --global user.email "friendly@ci"
    (cd deps/linux && git am ../../patches/linux/00*)
    mkdir releases
  script:
    - |
      case $GOARCH in
        amd64|arm64|riscv64)
          # Compile the slimmer kernel
          make -j${FDO_CI_CONCURRENT:-8} linux LINUX_SRC=deps/linux FEATURES="$BASE_FEATURES"
          mv out/linux-$LINUX_ARCH releases/linux-$LINUX_ARCH-qemu
          ;;
      esac

      # Decompress the riscv64 kernel until QEmu learns to boot gzip'ed images
      if [ -f "releases/linux-riscv64-qemu" ]; then
        mv releases/linux-riscv64-qemu releases/linux-riscv64-qemu.gz
        gzip -df releases/linux-riscv64-qemu.gz
      fi

    # Compile the full kernel
    - make -j${FDO_CI_CONCURRENT:-8} linux LINUX_SRC=deps/linux FEATURES="$FULL_FEATURES,ucode"
    - mv out/{linux-$LINUX_ARCH,linux-$LINUX_ARCH.*.cpio*} releases/


# STAGE: tests


.tests:
  stage: tests
  extends:
    - .run_in_container
  variables:
    B2C_KERNEL: /tmp/kernel
    QEMU: qemu-system-x86_64 -enable-kvm
  before_script:
    - |
      KERNEL_ARCH=${GOARCH/amd64/x86_64}  # convert amd64 into x86_64
      KERNEL_NAME=linux-${KERNEL_ARCH}
      # Try using the kernel we compiled (for releases), or pick the last release otherwise
      if [ -f releases/${KERNEL_NAME} ]; then
          cp releases/${KERNEL_NAME} $B2C_KERNEL
      else
          LATEST_RELEASE=$(curl ${CI_API_V4_URL}/projects/8765/releases | jq -r '.[0].tag_name')
          curl -L -o $B2C_KERNEL https://gitlab.freedesktop.org/mupuf/boot2container/-/releases/${LATEST_RELEASE}/downloads/${KERNEL_NAME}
      fi
    - cp -r ./config/keymaps/ /usr/share/keymaps/
    - export B2C_INITRD=releases/$CI_COMMIT_BRANCH/initramfs.linux_${GOARCH}.cpio.xz

.tests amd64:
  extends:
    - .tests
  variables:
    GOARCH: amd64

unit tests amd64:
  extends:
    - .tests amd64
  needs:
    - job: uroot
      artifacts: false
      parallel:
        matrix:
          - GOARCH: amd64
  script:
    - UNITTEST=1 INTEGRATION=0 VM2C=0 ./tests/tests.sh

integration tests amd64:
  extends:
    - .tests amd64
  needs:
    - job: linux
      parallel:
        matrix:
          - GOARCH: amd64
            LINUX_ARCH: x86_64
      optional: true
    - build amd64
  script:
    - UNITTEST=0 INTEGRATION=1 VM2C=0 ./tests/tests.sh
  tags:
    - kvm

vm2c tests amd64 (HEAD):
  extends:
    - .tests amd64
  needs:
    - job: linux
      parallel:
        matrix:
          - GOARCH: amd64
            LINUX_ARCH: x86_64
      optional: true
    - build amd64
  script:
    - export B2C_KERNEL B2C_INITRD
    - UNITTEST=0 INTEGRATION=0 VM2C=1 CACHE_DEVICE_VERSION=1 ./tests/tests.sh
  tags:
    - kvm

vm2c tests amd64 (latest release):
  extends:
    - .tests amd64
  needs:
    - job: uroot
      artifacts: false
      parallel:
       matrix:
         - GOARCH: amd64
  script:
    - unset B2C_KERNEL B2C_INITRD
    - UNITTEST=0 INTEGRATION=0 VM2C=1 CACHE_DEVICE_VERSION=1 ./tests/tests.sh
  tags:
    - kvm

# STAGE: release


release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
      when: manual
    - when: never
  script:
    - apk add curl
    - |
      set -eux
      assets=""
      while read FILENAME DESCRIPTION; do
            URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/release/${CI_COMMIT_TAG}/${FILENAME}"
            FILESIZE="$(du -h releases/$FILENAME | xargs | cut -d ' ' -f 1)"
            curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "releases/$FILENAME" "${URL}"
            assets="$assets --assets-link \"{\\\"name\\\":\\\"${FILENAME} (${DESCRIPTION}, ${FILESIZE}B)\\\",\\\"url\\\":\\\"${URL}\\\",\\\"link_type\\\":\\\"other\\\",\\\"filepath\\\":\\\"/${FILENAME}\\\"}\""
      done <<'EOD'
        linux-riscv64.headers.cpio.xz   RISCV64 Linux headers and Kbuild for out-of-tree driver support
        linux-riscv64.dtbs.cpio.xz      RISCV64 Linux device tree binaries
        linux-riscv64.firmware.cpio.xz  RISCV64 Linux firmware (only needed for WiFi support)
        linux-riscv64.modules.cpio.xz   RISCV64 Linux modules (only needed for WiFi support)
        linux-riscv64                   RISCV64 Linux build (EFI-compatible vmlinuz, experimental)
        linux-riscv64-qemu              RISCV64 Linux build (EFI-compatible vmlinux, experimental)), minified and uncompressed for QEMU
        initramfs.linux_riscv64.cpio.xz RISCV64 initramfs build (experimental)
        linux-arm64.headers.cpio.xz     Aarch64 Linux headers and Kbuild for out-of-tree driver support
        linux-arm64.dtbs.cpio.xz        Aarch64 Linux device tree binaries
        linux-arm64.firmware.cpio.xz    Aarch64 Linux firmware (only needed for WiFi support)
        linux-arm64.modules.cpio.xz     Aarch64 Linux modules (only needed for WiFi support)
        linux-arm64                     Aarch64 Linux build (EFI-compatible vmlinuz)
        linux-arm64-qemu                Aarch64 Linux build (EFI-compatible vmlinuz), minified for QEMU
        initramfs.linux_arm64.cpio.xz   Aarch64 initramfs build
        linux-x86_64.headers.cpio.xz    x86_64 Linux headers and Kbuild for out-of-tree driver support
        linux-x86_64.firmware.cpio.xz   x86_64 Linux firmware (only needed for WiFi support)
        linux-x86_64.modules.cpio.xz    x86_64 Linux modules (only needed for WiFi support)
        linux-x86_64                    x86_64 Linux build (PCBIOS/EFI)
        linux-x86_64-qemu               x86_64 Linux build (PCBIOS/EFI), minified for QEMU
        initramfs.linux_amd64.cpio.xz   x86_64 initramfs build
      EOD
      eval "release-cli create --name \"${CI_COMMIT_TAG} - Release in progress\" --description \"The description is currently being written. Come back in a bit.\" --tag-name \"${CI_COMMIT_TAG}\" $assets"
